from django.db import models
from django.contrib.auth.models import User

class Docente(models.Model):
    CEDULA = 'CC'
    CEDULA_E = 'CE'
    TIPO_DOC_CHOICES = [
        (CEDULA, 'Cedula Ciudadania'),
        (CEDULA_E, 'Cedula Extranjeria'),
    ]
    FEMENINO = 'FEM'
    MASCULINO = 'MAS'
    GEN_CHOICES = [
        (FEMENINO, 'Femenino'),
        (MASCULINO, 'Masculino'), 
    ]  
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    tipo_doc = models.CharField(max_length=2,
        choices=TIPO_DOC_CHOICES,
        default="CC",null=False)
    num_doc = models.IntegerField(null=False)
    genero = models.CharField(max_length=3,
        choices=GEN_CHOICES,
        default="FEM",null=False)
    
    def __str__(self):
        return self.usuario.first_name

    class Meta:
        app_label = 'home'

class Taller(models.Model):
    nombre = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    descripcion = models.CharField(max_length=100,null=False)
    codigo = models.IntegerField(null=False)
    capacidad = models.IntegerField(null=False)
    docente = models.ForeignKey(Docente,
                 related_name='docente_taller',
                 on_delete=models.CASCADE,
                 null=False)
    horario = models.CharField(max_length=100,null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'home'

class Aspirante(models.Model):
    CEDULA = 'CC'
    TARJETA = 'TI'
    CEDULA_E = 'CE'
    TIPO_DOC_CHOICES = [
        (CEDULA, 'Cedula Ciudadania'),
        (TARJETA, 'Tarjeta de identidad'),
        (CEDULA_E, 'Cedula Extranjeria'),
    ]
    FEMENINO = 'FEM'
    MASCULINO = 'MAS'
    GEN_CHOICES = [
        (FEMENINO, 'Femenino'),
        (MASCULINO, 'Masculino'),
    ]
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    edad = models.IntegerField(null=False)
    tipo_doc = models.CharField(max_length=2,
        choices=TIPO_DOC_CHOICES,
        default="TI",null=False)
    num_doc = models.IntegerField(default=0)
    genero = models.CharField(max_length=3,
        choices=GEN_CHOICES,
        default="FEM",null=False)
    colegio = models.CharField(max_length=100,null=False)
    ciudad = models.CharField(max_length=100,null=False)
    direccion = models.CharField(max_length=100,null=False)
    telefono = models.IntegerField(null=False)
    nombre_acudiente = models.CharField(max_length=100,null=False)
    telefono_acudiente = models.IntegerField(null=False)
    taller = models.ForeignKey(Taller,
                 related_name='taller_aspirante',
                 on_delete=models.CASCADE,
                 null=True)

    def __str__(self):
        return self.usuario.first_name

    class Meta:
        app_label = 'home'

class Registro(models.Model):

    aspirante = models.ForeignKey(Aspirante,related_name='asistencia_aspirante',on_delete=models.CASCADE,null=True)
    fecha = models.DateTimeField(null=True)
    taller = models.ForeignKey(Taller,related_name='asistencia_taller',on_delete=models.CASCADE,null=True)
    asistencia = models.BooleanField(default=True)

    def __str__(self):
        return self.taller.nombre

    class Meta:
        app_label = 'home'

