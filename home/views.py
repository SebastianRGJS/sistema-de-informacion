from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import Taller, Aspirante, Docente, Registro
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request, 'home/index.html')  

def home(request):
    return render(request, 'home/index.html')

def crearaspirante(request):
    # Obtiene la información de los talleres
    talleres = Taller.objects.all()
    
    # Crea el contexto
    contexto = {'talleres': talleres}

    return render(request,'home/pages/crear-aspirante.html',contexto)

def crear_taller_accion(request):
    
    # Obtiene la informacion del cliente
    valor_nombre = request.POST['nombre']
    valor_codigo = request.POST['codigo']
    valor_descripcion = request.POST['descripcion']
    valor_capacidad = request.POST['capacidad']
    valor_docente = Docente.objects.get(id = request.POST['docente'])
    valor_horario = request.POST['horario']

    # Guarda la categoria en la base de datos
    tal = Taller(nombre=valor_nombre,codigo=valor_codigo,descripcion=valor_descripcion,capacidad=valor_capacidad,
    docente=valor_docente,horario=valor_horario)
    tal.save()

    # Redirecciona a la pagina de talleres
    return HttpResponseRedirect(reverse('vertalleres'))

def crear_aspirante_accion(request):
    # Obtiene la informacion del cliente

    valor_edad = request.POST['edad']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_colegio = request.POST['colegio']
    valor_ciudad = request.POST['ciudad']
    valor_direccion = request.POST['direccion']
    valor_telefono = request.POST['telefono']
    valor_nombre_acudiente = request.POST['nombre_acudiente']
    valor_telefono_acudiente = request.POST['telefono_acudiente']
    valor_taller = Taller.objects.get(id = request.POST['taller'])

    u = User(username = request.POST['username'],first_name = request.POST['nombre']
        ,last_name = request.POST['apellido']
        ,email = request.POST['correo']
    )
    u.set_password(request.POST['num_doc'])

    u.save()

    # Guarda la categoria en la base de datos
    asp = Aspirante(edad=valor_edad,tipo_doc=valor_tipo_doc,num_doc=valor_num_doc,genero=valor_genero,
    colegio=valor_colegio,ciudad=valor_ciudad,
    direccion=valor_direccion,telefono=valor_telefono,
    nombre_acudiente=valor_nombre_acudiente,telefono_acudiente=valor_telefono_acudiente,usuario = u)
    asp.save()

    aspirantesel = Aspirante.objects.get(usuario=u)    
    reg = Registro(aspirante=aspirantesel,taller=valor_taller)
    reg.save()
    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('veraspirantes'))

def creartaller(request):
     # Obtiene la información de los talleres
    docentes = Docente.objects.all()

    # Crea el contexto
    contexto = {'docentes': docentes}

    return render(request, 'home/pages/Crear_taller.html', contexto)    

def tomarasistencia(request):
    # Obtiene la información de los talleres
    registros = Registro.objects.all()
    aspirantes = Aspirante.objects.all()
    talleres = Taller.objects.all()
    # Crea el contexto
    contexto = {'registros': registros,
    'talleres': talleres, 'aspirantes': aspirantes}

    return render(request,'home/pages/Tomar_asistencia.html',contexto)

def veraspirantes(request):
    # Obtiene los aspirantes
    aspirantes = Aspirante.objects.all()

    # Crea el contexto
    contexto = {'aspirantes': aspirantes}

    # Envia la informacion al template
    return render(request,'home/pages/visualizar-estudiantes.html',contexto)
    
def ver_aspirante(request, id):

    # Obtiene la informacion del aspirante
    aspirante = Aspirante.objects.get(pk=id)

    contexto = {'aspirante': aspirante}

    return render(request, 'home/pages/ver_aspirante.html', contexto)

def vertalleres(request):
    # Obtiene los talleres
    talleres = Taller.objects.all()

    # Crea el contexto
    contexto = {'talleres': talleres}

    # Envia la informacion al template
    return render(request, 'home/pages/Ver_talleres.html',contexto)

def ver_taller(request, id):

    # Obtiene la informacion del taller
    taller = Taller.objects.get(pk=id)

    contexto = {'taller': taller}

    return render(request, 'home/pages/ver_taller.html', contexto)

def modificar_taller(request,id):
   # Obtiene la informacion del taller
    taller = Taller.objects.get(pk=id)
    docentes = Docente.objects.all()

    contexto = {'taller': taller, 'docentes': docentes}

    return render(request, 'home/pages/modificar_taller.html', contexto)

def modificar_taller_accion(request):

    # Obtiene la informacion del cliente
    valor_id = request.POST['id']
    valor_nombre = request.POST['nombre']
    valor_descripcion = request.POST['descripcion']
    valor_codigo = request.POST['codigo']
    valor_capacidad = request.POST['capacidad']
    valor_docente = Docente.objects.get(id = request.POST['docente'])
    valor_horario = request.POST['horario']

    # Obtiene el taller de la base de datos
    taller = Taller.objects.get(pk=valor_id)

    # Modifica los valores
    taller.nombre = valor_nombre
    taller.descripcion = valor_descripcion
    taller.codigo = valor_codigo
    taller.capacidad = valor_capacidad
    taller.docente = valor_docente
    taller.horario = valor_horario

    # Guarda la categoria en la base de datos
    taller.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('vertalleres'))

def registro(request):
    # Obtiene la información de los talleres
    talleres = Taller.objects.all()

    # Crea el contexto
    contexto = {'talleres': talleres}

    return render(request,'home/pages/sign-up.html',contexto)

def crear_usuario_accion(request):
    # Obtiene la informacion del cliente

    valor_edad = request.POST['edad']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_colegio = request.POST['colegio']
    valor_ciudad = request.POST['ciudad']
    valor_direccion = request.POST['direccion']
    valor_telefono = request.POST['telefono']
    valor_nombre_acudiente = request.POST['nombre_acudiente']
    valor_telefono_acudiente = request.POST['telefono_acudiente']
    valor_taller = Taller.objects.get(id = request.POST['taller'])

    u = User(username = request.POST['username'],first_name = request.POST['nombre']
        ,last_name = request.POST['apellido']
        ,email = request.POST['correo']
    )
    u.set_password(request.POST['num_doc'])

    u.save()

    # Guarda la categoria en la base de datos
    asp = Aspirante(edad=valor_edad,tipo_doc=valor_tipo_doc,num_doc=valor_num_doc,genero=valor_genero,
    colegio=valor_colegio,ciudad=valor_ciudad,
    direccion=valor_direccion,telefono=valor_telefono,
    nombre_acudiente=valor_nombre_acudiente,telefono_acudiente=valor_telefono_acudiente,
    usuario = u)
    asp.save()   

    aspirantesel = Aspirante.objects.get(usuario=u)    
    reg = Registro(aspirante=aspirantesel,taller=valor_taller)
    reg.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('index'))

def recuperarpass(request):
    return render(request,'home/pages/forgot-password.html')

def verasistencia(request):

    return render(request,'home/pages/Ver_asistencia.html')

def activardesactivar(request):
    
    aspirantes = Aspirante.objects.all()
    docentes = Docente.objects.all()

    contexto = {'aspirantes': aspirantes, 'docentes': docentes}

    return render(request,'home/pages/activar-desactivar.html',contexto)

def activaraspirante(request,id):

    u = User.objects.get(pk=id)
    u.is_active = True
    u.save()

    return HttpResponseRedirect(reverse('activardesactivar'))

def desactivaraspirante(request,id):
    
    u = User.objects.get(pk=id)
    u.is_active = False
    u.save()

    return HttpResponseRedirect(reverse('activardesactivar'))

def modificaruser(request,id):
    aspirante = Aspirante.objects.get(pk=id)
    aspirantes = Aspirante.objects.all()
    talleres = Taller.objects.all()

    contexto = {'aspirante': aspirante, 'aspirantes':aspirantes, 'talleres':talleres}
    return render(request,'home/pages/modificar-usuario.html',contexto)

def modificar_aspirante_accion(request):
    valor_id = request.POST['id']
    valor_idd = request.POST['idd']
    valor_edad = request.POST['edad']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_colegio = request.POST['colegio']
    valor_ciudad = request.POST['ciudad']
    valor_direccion = request.POST['direccion']
    valor_telefono = request.POST['telefono']
    valor_nombre_acudiente = request.POST['nombre_acudiente']
    valor_telefono_acudiente = request.POST['telefono_acudiente']
    valor_taller = Taller.objects.get(id = request.POST['taller'])
    valor_username = request.POST['username']
    valor_first_name = request.POST['nombre']
    valor_last_name = request.POST['apellido']
    valor_email = request.POST['email']


    #obtiene el aspirante
    aspirante = Aspirante.objects.get(pk=valor_id)
    u = User.objects.get(pk=valor_idd)

    u.username = valor_username
    u.first_name = valor_first_name
    u.last_name = valor_last_name
    u.email = valor_email
    u.save()

    # Guarda la categoria en la base de datos
    aspirante.edad = valor_edad
    aspirante.tipo_doc = valor_tipo_doc
    aspirante.num_doc = valor_num_doc
    aspirante.genero = valor_genero
    aspirante.colegio = valor_colegio
    aspirante.ciudad = valor_ciudad
    aspirante.direccion = valor_ciudad
    aspirante.telefono = valor_telefono
    aspirante.nombre_acudiente = valor_nombre_acudiente
    aspirante.telefono_acudiente = valor_telefono_acudiente
    aspirante.taller = valor_taller
    aspirante.usuario = u

    aspirante.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('veraspirantes'))

def verdocentes(request):
    # Obtiene los aspirantes
    docentes = Docente.objects.all()

    # Crea el contexto
    contexto = {'docentes': docentes}

    # Envia la informacion al template
    return render(request,'home/pages/viualizar-docentes.html',contexto)

def crear_aspirante_admin(request):
    # Obtiene la información de los talleres
    talleres = Taller.objects.all()

    # Crea el contexto
    contexto = {'talleres': talleres}

    return render(request,'home/pages/aspirante_admin.html',contexto)

def crear_aspirante_admin_accion(request):
    # Obtiene la informacion del cliente

    valor_edad = request.POST['edad']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_colegio = request.POST['colegio']
    valor_ciudad = request.POST['ciudad']
    valor_direccion = request.POST['direccion']
    valor_telefono = request.POST['telefono']
    valor_nombre_acudiente = request.POST['nombre_acudiente']
    valor_telefono_acudiente = request.POST['telefono_acudiente']
    valor_taller = Taller.objects.get(id = request.POST['taller'])

    u = User(username = request.POST['username'],first_name = request.POST['nombre']
        ,last_name = request.POST['apellido']
        ,email = request.POST['correo']
    )
    u.set_password(request.POST['num_doc'])

    u.save()

    # Guarda la categoria en la base de datos
    asp = Aspirante(edad=valor_edad,tipo_doc=valor_tipo_doc,num_doc=valor_num_doc,genero=valor_genero,
    colegio=valor_colegio,ciudad=valor_ciudad,
    direccion=valor_direccion,telefono=valor_telefono,
    nombre_acudiente=valor_nombre_acudiente,telefono_acudiente=valor_telefono_acudiente,
    usuario = u)
    asp.save()   

    aspirantesel = Aspirante.objects.get(usuario=u)    
    reg = Registro(aspirante=aspirantesel,taller=valor_taller)
    reg.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('activardesactivar'))

def crear_docente_admin(request):

    return render(request,'home/pages/docente_admin.html')

def crear_docente_admin_accion(request):
    # Obtiene la informacion del cliente

    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']

    u = User(username = request.POST['username'],first_name = request.POST['nombre']
        ,last_name = request.POST['apellido']
        ,email = request.POST['correo']
    )
    u.set_password(request.POST['num_doc'])

    u.save()

    # Guarda la categoria en la base de datos
    doc = Docente(tipo_doc=valor_tipo_doc,num_doc=valor_num_doc,genero=valor_genero,usuario = u)
    doc.save()   

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('activardesactivar'))

def veraspirantes_admin(request):
    # Obtiene los aspirantes
    aspirantes = Aspirante.objects.all()

    # Crea el contexto
    contexto = {'aspirantes': aspirantes}

    # Envia la informacion al template
    return render(request,'home/pages/visualizar-estudiantes_admin.html',contexto)
    
def ver_aspirante_admin(request, id):

    # Obtiene la informacion del aspirante
    aspirante = Aspirante.objects.get(pk=id)

    contexto = {'aspirante': aspirante}

    return render(request, 'home/pages/ver_aspirante_admin.html', contexto)

def modificaruser_admin(request,id):
    aspirante = Aspirante.objects.get(pk=id)
    aspirantes = Aspirante.objects.all()
    talleres = Taller.objects.all()

    contexto = {'aspirante': aspirante, 'aspirantes':aspirantes, 'talleres':talleres}
    return render(request,'home/pages/modificar-usuario_admin.html',contexto)

def modificar_aspirante_admin_accion(request):
    valor_id = request.POST['id']
    valor_idd = request.POST['idd']
    valor_edad = request.POST['edad']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_colegio = request.POST['colegio']
    valor_ciudad = request.POST['ciudad']
    valor_direccion = request.POST['direccion']
    valor_telefono = request.POST['telefono']
    valor_nombre_acudiente = request.POST['nombre_acudiente']
    valor_telefono_acudiente = request.POST['telefono_acudiente']
    valor_taller = Taller.objects.get(id = request.POST['taller'])
    valor_username = request.POST['username']
    valor_first_name = request.POST['nombre']
    valor_last_name = request.POST['apellido']
    valor_email = request.POST['email']


    #obtiene el aspirante
    aspirante = Aspirante.objects.get(pk=valor_id)
    u = User.objects.get(pk=valor_idd)

    u.username = valor_username
    u.first_name = valor_first_name
    u.last_name = valor_last_name
    u.email = valor_email
    u.save()

    # Guarda la categoria en la base de datos
    aspirante.edad = valor_edad
    aspirante.tipo_doc = valor_tipo_doc
    aspirante.num_doc = valor_num_doc
    aspirante.genero = valor_genero
    aspirante.colegio = valor_colegio
    aspirante.ciudad = valor_ciudad
    aspirante.direccion = valor_ciudad
    aspirante.telefono = valor_telefono
    aspirante.nombre_acudiente = valor_nombre_acudiente
    aspirante.telefono_acudiente = valor_telefono_acudiente
    aspirante.taller = valor_taller
    aspirante.usuario = u

    aspirante.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('veraspirantes_admin'))

def borraruser_admin(request,id):
    
    aspirante = Aspirante.objects.get(pk=id)
    # Crea el contexto
    aspirante.delete()

    # Envia la informacion al template
    return HttpResponseRedirect(reverse('veraspirantes_admin'))

def borrardocente_admin(request,id):
    
    docente = Docente.objects.get(pk=id)
    # Crea el contexto
    docente.delete()

    # Envia la informacion al template
    return HttpResponseRedirect(reverse('verdocentes'))

def borrartaller_admin(request,id):
    
    taller = Taller.objects.get(pk=id)
    # Crea el contexto
    taller.delete()

    # Envia la informacion al template
    return HttpResponseRedirect(reverse('vertalleres_admin'))

def hacer_login(request):

    # Obtiene los datos de autenticacion
    usuario = request.POST['usuario']
    password = request.POST['password']

    # Obtiene el usuario con los datos de autenticacion
    user = authenticate(username=usuario, password=password)

    # Autentica el usuario
    if user is not None:
        login(request, user)

    try:
        doc = Docente.objects.get(usuario=user)
        return HttpResponseRedirect(reverse('crearaspirante'))
        # Redirecciona a la pagina de categorias
    except: 
        return HttpResponseRedirect(reverse('activardesactivar'))
    else:
        return HttpResponse('<h1>ERROR!!!</h1>')    

def ver_docente(request, id):

    # Obtiene la informacion del taller
    docente = Docente.objects.get(pk=id)

    contexto = {'docente': docente}

    return render(request, 'home/pages/ver_docente.html', contexto)

def modificar_docente(request,id):
   # Obtiene la informacion del taller
    docente = Docente.objects.get(pk=id)

    contexto = {'docente': docente}

    return render(request, 'home/pages/modificar_docente.html', contexto)

def modificar_docente_accion(request):

    valor_id = request.POST['id']
    valor_idd = request.POST['idd']
    valor_tipo_doc = request.POST['tipo_doc']
    valor_num_doc = request.POST['num_doc']
    valor_genero = request.POST['genero']
    valor_username = request.POST['username']
    valor_first_name = request.POST['nombre']
    valor_last_name = request.POST['apellido']
    valor_email = request.POST['email']

    #obtiene el docente
    docente = Docente.objects.get(pk=valor_id)
    u = User.objects.get(pk=valor_idd)

    u.username = valor_username
    u.first_name = valor_first_name
    u.last_name = valor_last_name
    u.email = valor_email
    u.save()

    # Guarda la categoria en la base de datos
    docente.tipo_doc = valor_tipo_doc
    docente.num_doc = valor_num_doc
    docente.genero = valor_genero
    docente.usuario = u

    docente.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('verdocentes'))

def creartaller_admin(request):
     # Obtiene la información de los talleres
    docentes = Docente.objects.all()

    # Crea el contexto
    contexto = {'docentes': docentes}

    return render(request, 'home/pages/crear_taller_admin.html', contexto)    

def crear_taller_admin_accion(request):
    
    # Obtiene la informacion del cliente
    valor_nombre = request.POST['nombre']
    valor_codigo = request.POST['codigo']
    valor_descripcion = request.POST['descripcion']
    valor_capacidad = request.POST['capacidad']
    valor_docente = Docente.objects.get(id = request.POST['docente'])
    valor_horario = request.POST['horario']

    # Guarda la categoria en la base de datos
    tal = Taller(nombre=valor_nombre,codigo=valor_codigo,descripcion=valor_descripcion,capacidad=valor_capacidad,
    docente=valor_docente,horario=valor_horario)
    tal.save()

    # Redirecciona a la pagina de talleres
    return HttpResponseRedirect(reverse('vertalleres_admin'))

def vertalleres_admin(request):
    # Obtiene los talleres
    talleres = Taller.objects.all()

    # Crea el contexto
    contexto = {'talleres': talleres}

    # Envia la informacion al template
    return render(request, 'home/pages/ver_talleres_admin.html',contexto)

def ver_taller_admin(request, id):

    # Obtiene la informacion del taller
    taller = Taller.objects.get(pk=id)

    contexto = {'taller': taller}

    return render(request, 'home/pages/ver_taller_admin.html', contexto)

def modificar_taller_admin(request,id):
   # Obtiene la informacion del taller
    taller = Taller.objects.get(pk=id)
    docentes = Docente.objects.all()

    contexto = {'taller': taller, 'docentes': docentes}

    return render(request, 'home/pages/modificar_taller_admin.html', contexto)

def modificar_taller_admin_accion(request):

    # Obtiene la informacion del cliente
    valor_id = request.POST['id']
    valor_nombre = request.POST['nombre']
    valor_descripcion = request.POST['descripcion']
    valor_codigo = request.POST['codigo']
    valor_capacidad = request.POST['capacidad']
    valor_docente = Docente.objects.get(id = request.POST['docente'])
    valor_horario = request.POST['horario']

    # Obtiene el taller de la base de datos
    taller = Taller.objects.get(pk=valor_id)

    # Modifica los valores
    taller.nombre = valor_nombre
    taller.descripcion = valor_descripcion
    taller.codigo = valor_codigo
    taller.capacidad = valor_capacidad
    taller.docente = valor_docente
    taller.horario = valor_horario

    # Guarda la categoria en la base de datos
    taller.save()

    # Redirecciona a la pagina de categorias
    return HttpResponseRedirect(reverse('vertalleres_admin'))

def hacer_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))        