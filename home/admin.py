from django.contrib import admin
from .models import Taller,Aspirante,Docente,Registro

admin.site.register(Taller)
admin.site.register(Aspirante)
admin.site.register(Docente)
admin.site.register(Registro)