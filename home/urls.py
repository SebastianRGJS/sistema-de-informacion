from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('hacer_login/', views.hacer_login, name='login'),
    path('logout/', views.hacer_logout, name='logout'),

    path('crearaspirante/',views.crearaspirante, name='crearaspirante'),
    path('crearaspirante/crear_aspirante_accion', views.crear_aspirante_accion, name='crear_aspirante_accion'),

    path('creartaller/',views.creartaller, name='creartaller'),
    path('creartaller/crear_taller_accion', views.crear_taller_accion, name='crear_taller_accion'),

    path('tomarasistencia/',views.tomarasistencia, name='tomarasistencia'),

    path('veraspirantes/',views.veraspirantes, name='veraspirantes'),
    path('verapirantes/<int:id>/',views.ver_aspirante, name='ver_aspirante'),
    path('modificaruser/<int:id>/',views.modificaruser, name='modificaruser'),
    path('modificaruser/modificar/',views.modificar_aspirante_accion, name='modificar_aspirante_accion'),

    path('vertalleres/',views.vertalleres, name='vertalleres'),
    path('vertalleres/<int:id>/',views.ver_taller, name='vertaller'),
    path('talleres/modificar/<int:id>/', views.modificar_taller, name='modificar_taller'),
    path('talleres/modificar_taller', views.modificar_taller_accion, name='modificar_taller_accion'),

    path('registro/',views.registro, name='registro'),
    path('registro/crearusuario', views.crear_usuario_accion, name='crear_usuario_accion'),
    path('verasistencia/',views.verasistencia, name='verasistencia'),
    
    path('activardesactivar/',views.activardesactivar, name='activardesactivar'),
    path('crearusuarioaspirante/',views.crear_aspirante_admin, name='crear_aspirante_admin'),
    path('crearusuarioaspirante/admin',views.crear_aspirante_admin_accion, name='crear_aspirante_admin_accion'),
    path('crearusuariodocente/',views.crear_docente_admin, name='crear_docente_admin'),
    path('crearusuarioadocente/admin',views.crear_docente_admin_accion, name='crear_docente_admin_accion'),
    
    path('veraspirantesadmin/',views.veraspirantes_admin, name='veraspirantes_admin'),
    path('verapirantesadmin/<int:id>/',views.ver_aspirante_admin, name='ver_aspirante_admin'),
    path('modificaruseradmin/<int:id>/',views.modificaruser_admin, name='modificaruser_admin'),
    path('modificaruseradmin/modificar/',views.modificar_aspirante_admin_accion, name='modificar_aspirante_admin_accion'),
    
    path('veraspirantesadmin/borraruser/<int:id>',views.borraruser_admin, name='borraruser_admin'),
    path('verdocentes/borraruser/<int:id>',views.borrardocente_admin, name='borrardocente_admin'),
    path('vertalleres_admin/borrartaller/<int:id>',views.borrartaller_admin, name='borrartaller_admin'),


    path('verdocentes/',views.verdocentes, name='verdocentes'),  
    path('verdocentes/<int:id>/',views.ver_docente, name='ver_docente'),
    path('docentes/modificar/<int:id>/', views.modificar_docente, name='modificar_docente'),
    path('docentes/modificar_docente', views.modificar_docente_accion, name='modificar_docente_accion'),
    
    path('creartaller_admin/',views.creartaller_admin, name='creartaller_admin'),
    path('creartaller/crear_taller_admin_accion', views.crear_taller_admin_accion, name='crear_taller_admin_accion'),
    path('vertalleres_admin/',views.vertalleres_admin, name='vertalleres_admin'),
    path('vertalleres_admin/<int:id>/',views.ver_taller_admin, name='vertaller_admin'),
    path('talleres_admin/modificar/<int:id>/', views.modificar_taller_admin, name='modificar_taller_admin'),
    path('talleres_admin/modificar_taller', views.modificar_taller_admin_accion, name='modificar_taller_admin_accion'),

    path('activaraspirante/<int:id>',views.activaraspirante, name='activaraspirante'), 
    path('desactivaraspirante/<int:id>',views.desactivaraspirante, name='desactivaraspirante'), 

    path('recuperarpass/',views.recuperarpass, name='recuperarpass'),  
    path('crearuser/',views.verdocentes, name='modificaruser'),
    path('crearuser/',views.verdocentes, name='crearuser'),
    path('crearuser/',views.verdocentes, name='editartaller'),
    path('crearuser/',views.verdocentes, name='generarreporte'),

    
    
]